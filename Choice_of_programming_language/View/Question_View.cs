﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Choice_of_programming_language
{
    public partial class Question_View : Form
    {
        // Строка вывода в GUI доп. инф. - Информация
        private const String Str_Information = "Система поддержки принятия решений\n"
                                            + "Выбор языка программирования\n"
                                            + "Выполнил: студент группы РБП-108м, Загидуллин Д.И.\n"
                                            + "Проверил: Сакаев Р.Д.\n"
                                            + "Уфа, УГАТУ, 2019 год\n"
                                            + "Все права защищены.";

        // Строка вывода в GUI доп. инф. - Справка
        private const String Str_Help = "Данный программный продукт помогает любому пользлвателю выбрать язык программирования\n"
                                      + "Тут еще нужно написать руководство пользователя, но я ленивый";

        // GUI по доп. инф.
        private Information_View information_View;

        // Контроллер класса
        private Questions_Controller qustion_Controller;

        // Флаг отвечающий за одновременный выбор
        private bool simultaneous_Selection;

        // Флаг отвечающий за последний вопрос
        private bool end;

        // Номер ответа по основному вопросу
        private int count_General_Question = -1;

        // Номер вопроса
        private int count_Question;

        // Массив выбранных ответов
        private bool[] answer;

        // Массив коэффициентов
        private int[] coefficient;

        // Массив показа количества ответов
        private bool[] checks;

        // Массив заполнения вопроса и ответов
        private String[] names;

        // Контсруктор класса
        public Question_View()
        {
            InitializeComponent();                                                                                                                      // Инициализация компонентов
            Load_Qustion();                                                                                                                             // Загрузка вопроса
        }

        // Загрузка класса
        private void Question_Load(object sender, EventArgs e) => this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(Exist);            // Выключение всей программы по кресту

        // Загрузка вопроса - основной метод
        private void Load_Qustion()
        {
            if (this.count_General_Question == -1)
                Init();                                                                                                                                // Инициализация переменных

            if (end == false)
            {
                this.qustion_Controller.Start(this.count_General_Question, this.count_Question, this.answer, this.end);                                 // Запуск контроллера вопросов
                Update_Data();                                                                                                                          // Обновление информации на форме
            }
            else
            {
                this.qustion_Controller.Start(this.count_General_Question, this.count_Question, this.answer, this.end);                                 // Запуск контроллера вопросов
                button2.Visible = false;
                button3.Visible = false;
            }
        }

        // Инициализация переменных
        private void Init()
        {
            this.information_View = new Information_View();                                                                                             // Информационное окно
            this.qustion_Controller = new Questions_Controller(this);                                                                                   // Создание экземпляра контроллера вопросов

            this.count_General_Question = 0;                                                                                                            // Ответ на основной вопрос
            this.count_Question = 1;                                                                                                                    // Номер ответа активного вопроса

            this.end = false;                                                                                                                           // Флаг отвечающий за последний вопрос
            this.simultaneous_Selection = true;                                                                                                         // Флаг отвечающий за одновременный выбор ответов

            this.answer = new bool[6];
            for (int i = 0; i < this.answer.Length; i++) this.answer[i] = false;                                                                        // Инициализация массива ответов

            this.coefficient = new int[5];
            for (int i = 0; i < coefficient.Length; i++) coefficient[i] = 0;                                                                            // Инициализация массива коэфициентов

            this.checks = new bool[6];
            for (int i = 0; i < this.checks.Length; i++) this.checks[i] = false;                                                                        // Инициализация массива показа количества ответов

            this.names = new String[7];
            for (int i = 0; i < this.names.Length; i++) names[i] = null;                                                                                // Инициализация массива заполнения вопроса и ответов
        }

        // Обновление имеющейся информации
        public void Set_Update_Information(String[] names, bool[] checks, int[] coefficient, bool simulation, bool end)
        {
            this.names = names;
            this.checks = checks;
            this.coefficient = coefficient;
            this.simultaneous_Selection = simulation;
            this.end = end;
        }

        // Обновление информации на форме
        private void Update_Data()
        {
            Init_Check_Box();                                                                                                                           // Сброс чекбоксов
            Set_Text_For_Elements(names);                                                                                                               // Обновление вопроса и ответов
            Set_Visible_Check(checks);                                                                                                                  // Обновление количества показанных вопросов
            Update_Coefficient(coefficient);                                                                                                            // Обновление коэффициентов
        }

        // Сброс чекбоксов
        public void Init_Check_Box()
        {
            checkBox1.Checked = false;                                                                                                                  // Первый чекбокс не нажат
            checkBox2.Checked = false;                                                                                                                  // Второй чекбокс не нажат
            checkBox3.Checked = false;                                                                                                                  // Третий чекбокс не нажат
            checkBox4.Checked = false;                                                                                                                  // Четвертый чекбокс не нажат
            checkBox5.Checked = false;                                                                                                                  // Пятый чекбокс не нажат
            checkBox6.Checked = false;                                                                                                                  // Шестой чекбокс не нажат
        }

        // Обновление отображения ответов 
        public void Set_Visible_Check(bool[] data)
        {
            if (data[0]) checkBox1.Visible = true;                                                                                                      // Первый чекбокс виден                                       
                else checkBox1.Visible = false;                                                                                                         // Первый чекбокс не виден

            if (data[1]) checkBox2.Visible = true;                                                                                                      // Второй чекбокс виден
                else checkBox2.Visible = false;                                                                                                         // Второй чекбокс не виден

            if (data[2]) checkBox3.Visible = true;                                                                                                      // Третий чекбокс виден
                else checkBox3.Visible = false;                                                                                                         // Третий чекбокс не виден

            if (data[3]) checkBox4.Visible = true;                                                                                                      // Четвертый чекбокс виден
                else checkBox4.Visible = false;                                                                                                         // Четвертый чекбокс не виден

            if (data[4]) checkBox5.Visible = true;                                                                                                      // Пятый чекбокс виден
                else checkBox5.Visible = false;                                                                                                         // Пятый чекбокс не виден

            if (data[5]) checkBox6.Visible = true;                                                                                                      // Шестой чекбокс виден
                else checkBox6.Visible = false;                                                                                                         // Шестой чекбокс не виден
        }

        // Обновление коэффициентов 
        public void Update_Coefficient(int[] data)
        {
            label5.Text = data[0].ToString();                                                                                                           // Коэффициент Python
            label6.Text = data[1].ToString();                                                                                                           // Коэффициент Java
            label8.Text = data[2].ToString();                                                                                                           // Коэффициент C#
            label10.Text = data[3].ToString();                                                                                                          // Коэффициент PHP
            label12.Text = data[4].ToString();                                                                                                          // Коэффициент R
        }

        // Заполнение элементов формы данными
        public void Set_Text_For_Elements(String[] data)
        {
            if (simultaneous_Selection) groupBox1.Text = "Выберите один вариант ответа";                                                                // Обновление информации о выборе ответа
                else groupBox1.Text = "Выберите один или более вариантов ответа";                                                                       // Обновление информации о выборе ответа

            richTextBox1.Text = data[0];                                                                                                                // Заполнение вопроса
            richTextBox1.SelectionAlignment = HorizontalAlignment.Center;                                                                               // Отцентрирование текста

            if (data[1] != null) checkBox1.Text = data[1];                                                                                              // Заполнение первого ответа
            if (data[2] != null) checkBox2.Text = data[2];                                                                                              // Заполнение второго ответа
            if (data[3] != null) checkBox3.Text = data[3];                                                                                              // Заполнение третьего ответа
            if (data[4] != null) checkBox4.Text = data[4];                                                                                              // Заполнение четвертого ответа
            if (data[5] != null) checkBox5.Text = data[5];                                                                                              // Заполнение пятого ответа
            if (data[6] != null) checkBox6.Text = data[6];                                                                                              // Заполнение шестого ответа
        }

        // Считывание ответов
        private void Recalculate_Answer()
        {
            for(int i = 0; i < this.answer.Length; i++) this.answer[i] = false;                                                                         // Обнуление ответов

            if (checkBox1.Checked) this.answer[0] = true;                                                                                               // Если нажат ответ один
            if (checkBox2.Checked) this.answer[1] = true;                                                                                               // Если нажат ответ два
            if (checkBox3.Checked) this.answer[2] = true;                                                                                               // Если нажат ответ три
            if (checkBox4.Checked) this.answer[3] = true;                                                                                               // Если нажат ответ четыре
            if (checkBox5.Checked) this.answer[4] = true;                                                                                               // Если нажат ответ пять
            if (checkBox6.Checked) this.answer[5] = true;                                                                                               // Если нажат ответ шесть
        }

        // Проверка чекбоксов на выбор только одного элемента (при необходимости)
        private bool Check_Checks()
        {
            int count = 0;                                                                                                                              // Обнуляем параметр отвечающий за количество выбранных
            if (checkBox1.Checked) count++;                                                                                                             // Если выбран первый чекбокс увеличиваем параметр
            if (checkBox2.Checked) count++;                                                                                                             // Если выбран второй чекбокс увеличиваем параметр
            if (checkBox3.Checked) count++;                                                                                                             // Если выбран третий чекбокс увеличиваем параметр
            if (checkBox4.Checked) count++;                                                                                                             // Если выбран четвертый чекбокс увеличиваем параметр
            if (checkBox5.Checked) count++;                                                                                                             // Если выбран пятый чекбокс увеличиваем параметр
            if (checkBox6.Checked) count++;                                                                                                             // Если выбран шестой чекбокс увеличиваем параметр

            if (count != 1)
            {
                if (count == 0)
                {
                    View_Warning_Sms("Пожайлуста выберите хотя бы один ответ");                                                                         // Вывод предупреждения
                }
                else
                {
                    View_Warning_Sms("Пожайлуста выберите только один ответ");                                                                          // Вывод предупреждения
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        // Показ окна "Информация" с нужным наполнением
        private void Show_Information_View(String str)
        {
            information_View.Set_Text(str);                                                                                                             // Заполнение формы "Информация" нужными данными
            information_View.ShowDialog();                                                                                                              // Показ формы информации
        }

        // Считывание ответа на первый (основной) вопрос
        private void Read_General_Question()
        {
            if (checkBox1.Checked) this.count_General_Question = 1;
            if (checkBox2.Checked) this.count_General_Question = 2;
            if (checkBox3.Checked) this.count_General_Question = 3;
        }

        // Метод завершения работы программы
        public void Exist(object sender, EventArgs e) => Application.Exit();

        // Срабатывает при ответе на последний вопрос - показывает кнопку с результатами
        public void Set_Visible_Result()
        {
            groupBox1.Visible = false;
            label1.Text = "";
            label2.Text = "Результат";

            String result = "Поздравляем, вы прошли тест\nПо видимому вам необходим язык программирования:\n";
            int max = this.coefficient.Max();
            if (coefficient[0] == max) result += "Python\n";
            if (coefficient[1] == max) result += "Java\n";
            if (coefficient[2] == max) result += "C#\n";
            if (coefficient[3] == max) result += "PGP\n";
            if (coefficient[4] == max) result += "R\n";

            richTextBox1.Text = result;
            //richTextBox1.SelectionAlignment = HorizontalAlignment.Center;                                                                               // Отцентрирование текста
        }

        /*--------------------------------------КНОПКИ-------------------------------------*/
        
        // Нажатие - Кнопка "Вперед"
        private void button2_Click_1(object sender, EventArgs e)                                                                                        // Вперед
        {
            if (this.count_General_Question != 0)
            {
                Recalculate_Answer();
                if (simultaneous_Selection)
                {
                    if (Check_Checks())
                    {
                        
                        Load_Qustion();                                                                                                                 // Загрузка вопроса
                        this.count_Question++;                                                                                                          // Увеличиваем индес вопроса
                    }
                }
                else
                {
                    
                    Load_Qustion();                                                                                                                     // Загрузка вопроса
                    this.count_Question++;                                                                                                              // Увеличиваем индекс вопроса
                }
            }
            else
            {
                if (Check_Checks()) {
                    
                    Read_General_Question();
                    Load_Qustion();
                    this.count_Question++;
                }
            }
        }

        // Нажатие - Кнопка в меню "Начать заново"
        private void начатьЗановоToolStripMenuItem_Click(object sender, EventArgs e)                                                                    // Начать заново
        {
            this.count_General_Question = -1;                                                                                                           // Обнулить основной коэффициент
            button2.Visible = true;
            button5.Visible = false;
            groupBox1.Visible = true;
            Load_Qustion();                                                                                                                             // Запуск основной функции с обнуленным коэффициентов

        }

        // Нажатие - Кнопка "Результат"
        private void button5_Click(object sender, EventArgs e) { }

        // Нажатие - Кнопка в меню "Выход из программы"
        private void выходИзПрограммыToolStripMenuItem_Click(object sender, EventArgs e) => Exist(null, null);                                          // Выход из программы

        // Нажатие - Кнопка "Закрыть"
        private void button1_Click(object sender, EventArgs e) => Exist(null, null);                                                                    // Закрыть

        // Нажатие - Кнопка "Для преподавателя (Вкл\Выкл)"
        private void button4_Click(object sender, EventArgs e) => panel6.Visible = !panel6.Visible;                                                     // Для преподавателя (Вкл\Выкл)

        // Нажатие - Кнопка в меню "Информация"
        private void справкаToolStripMenuItem_Click(object sender, EventArgs e) => Show_Information_View(Str_Information);                              // Информация

        // Нажатие - Кнопка в меню "Справка"
        private void справкаToolStripMenuItem1_Click(object sender, EventArgs e) => Show_Information_View(Str_Help);                                    // Справка
        /*---------------------------------------------------------------------------------*/

        /*------------------------------------СООБЩЕНИЯ------------------------------------*/
        // Вывод сообщения об ошибке 
        public void View_Error_Sms(String str)
        {
            str += "Пожайлуста сообщите об этом администратору (то есть Дмитрию)";
            MessageBox.Show(str, "Ошибка");
        }

        // Вывод предупреждения 
        public void View_Warning_Sms(String str)
        {
            MessageBox.Show(str, "Предупреждение");
        }

        // Вывод информационного сообщения 
        public void View_Inforation_Sms(String str)
        {
            MessageBox.Show(str, "Информация");
        }
        /*---------------------------------------------------------------------------------*/

        /* Не используемые функции, но случайно нажатые */
        private void checkBox1_CheckedChanged(object sender, EventArgs e) { }

        private void checkBox2_CheckedChanged(object sender, EventArgs e) { }

        private void checkBox3_CheckedChanged(object sender, EventArgs e) { }

        private void checkBox4_CheckedChanged(object sender, EventArgs e) { }

        private void checkBox5_CheckedChanged(object sender, EventArgs e) { }

        private void checkBox6_CheckedChanged(object sender, EventArgs e) { }

        private void button2_Click(object sender, EventArgs e) { }

        private void groupBox1_Enter(object sender, EventArgs e) { }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e) { }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e) { }

        private void button3_Click(object sender, EventArgs e) { }

        private void groupBox1_Enter_1(object sender, EventArgs e) { }
        /*----------------------------------------------*/
    }
}
