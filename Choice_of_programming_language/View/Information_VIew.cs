﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Choice_of_programming_language
{
    public partial class Information_View : Form
    {
        public Information_View()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Information_Load(object sender, EventArgs e)
        {

        }

        public void Set_Text(String str)
        {
            richTextBox1.Text = str;
        }
    }
}
