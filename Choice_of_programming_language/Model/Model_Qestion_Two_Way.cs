﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice_of_programming_language.Model
{
    class Model_Qestion_Two_Way
    {
        // Данные первого вопроса
        private String[] name_One_Qestion = new string[7];

        // Данные второго вопроса
        private String[] name_Two_Qestion = new string[7];

        // Данные третьего вопроса
        private String[] name_Three_Qestion = new string[7];

        // Данные четвертого вопроса
        private String[] name_Four_Qestion = new string[7];

        // Данные пятого вопроса
        private String[] name_Five_Qestion = new string[7];

        // Данные шестого вопроса
        private String[] name_Six_Qestion = new string[7];

        // Конструктор класса
        public Model_Qestion_Two_Way()
        {
            Init_Names_Massive();                                                                                                                                           // Инициализация переменных
        }

        // Инициализация данных по вопросам
        private void Init_Names_Massive()
        {
            name_One_Qestion[0] = "При решении различного рода задач часто ли вы пользуететсь интернетом?";
            name_One_Qestion[1] = "Не пользуюсь совсем";
            name_One_Qestion[2] = "Да, пользуюсь";
            name_One_Qestion[3] = "Предпочитаю проверенную документацию";
            name_One_Qestion[4] = null;
            name_One_Qestion[5] = null;
            name_One_Qestion[6] = null;

            name_Two_Qestion[0] = "Используете ли вы различные паттерны (шаблоны) программирования?\nШаблон проектирования или паттерн (англ. design pattern) в разработке программного обеспечения — повторяемая архитектурная конструкция, представляющая собой решение проблемы проектирования в рамках некоторого часто возникающего контекста";
            name_Two_Qestion[1] = "Нет";
            name_Two_Qestion[2] = "Только начал изучать";
            name_Two_Qestion[3] = "Да";
            name_Two_Qestion[4] = null;
            name_Two_Qestion[5] = null;
            name_Two_Qestion[6] = null;

            name_Three_Qestion[0] = "Применяете ли вы методологию ООП?\n(Объектно - ориентированное программирование – методология программирования, основанная на представлении программы в виде совокупности объектов, каждый из которых является экземпляром определённого класса, а классы образуют иерархию наследования)";
            name_Three_Qestion[1] = "Нет";
            name_Three_Qestion[2] = "Предпочтительне";
            name_Three_Qestion[3] = "Да";
            name_Three_Qestion[4] = null;
            name_Three_Qestion[5] = null;
            name_Three_Qestion[6] = null;

            name_Four_Qestion[0] = "На сколько пользователей ориентирован ваш программный продукт?";
            name_Four_Qestion[1] = "Персональный программный продукт";
            name_Four_Qestion[2] = "Для друзей";
            name_Four_Qestion[3] = "Для работы";
            name_Four_Qestion[4] = "Для общего пользования";
            name_Four_Qestion[5] = null;
            name_Four_Qestion[6] = null;

            name_Five_Qestion[0] = "Собираетесь ли вы использовать базу данных для хранения и обработки информации?";
            name_Five_Qestion[1] = "Да";
            name_Five_Qestion[2] = "Нет";
            name_Five_Qestion[3] = null;
            name_Five_Qestion[4] = null;
            name_Five_Qestion[5] = null;
            name_Five_Qestion[6] = null;

            name_Six_Qestion[0] = "Необходима ли кроссплатформенность?";
            name_Six_Qestion[1] = "Нет";
            name_Six_Qestion[2] = "Предпочтительнее";
            name_Six_Qestion[3] = "Да";
            name_Six_Qestion[4] = null;
            name_Six_Qestion[5] = null;
            name_Six_Qestion[6] = null;
        }

        // Получения данных о вопросе по его номеру
        public String[] Set_Names_Massive(int count_Question)
        {
            if (count_Question == 1) return this.name_One_Qestion;
            if (count_Question == 2) return this.name_Two_Qestion;
            if (count_Question == 3) return this.name_Three_Qestion;
            if (count_Question == 4) return this.name_Four_Qestion;
            if (count_Question == 5) return this.name_Five_Qestion;
            if (count_Question == 6) return this.name_Six_Qestion;

            return null;
        }

        // Переподсчет коэффициентов
        public int[] Get_Coefficient(int count_Question, bool[] answer, int[] coefficient)
        {
            if (count_Question == 2) return Get_Coefficient_One_Question(answer, coefficient);
            if (count_Question == 3) return Get_Coefficient_Two_Question(answer, coefficient);
            if (count_Question == 4) return Get_Coefficient_Three_Question(answer, coefficient);
            if (count_Question == 5) return Get_Coefficient_Four_Question(answer, coefficient);
            if (count_Question == 6) return Get_Coefficient_Five_Question(answer, coefficient);
            if (count_Question == 7) return Get_Coefficient_Seven_Question(answer, coefficient);

            return null;
        }

        // Переподсчет коэффициентов при первом вопросе
        private int[] Get_Coefficient_One_Question(bool[] answer, int[] coefficient)
        {
            if (answer[0])
            {
                coefficient[0] += 1;
                coefficient[1] += 1;
                coefficient[2] += 1;
            }

            if (answer[1])
            {
                coefficient[0] += 1;
                coefficient[1] += 1;
                coefficient[2] += 1;
                coefficient[3] += 1;
            }

            if (answer[2])
            {
                coefficient[0] += 3;
                coefficient[1] += 3;
            }

            return coefficient;
        }

        // Переподсчет коэффициентов при втором вопросе
        private int[] Get_Coefficient_Two_Question(bool[] answer, int[] coefficient)
        {
            if (answer[0])
            {
                coefficient[3] += 1;
                coefficient[4] += 1;
            }

            if (answer[1])
            {
                coefficient[1] += 2;
                coefficient[2] += 1;
            }

            if (answer[2])
            {
                coefficient[0] += 1;
                coefficient[1] += 1;
                coefficient[2] += 1;
            }

            return coefficient;
        }

        // Переподсчет коэффициентов при третьем вопросе
        private int[] Get_Coefficient_Three_Question(bool[] answer, int[] coefficient)
        {
            if (answer[0])
            {
                coefficient[0] += 1;
                coefficient[1] -= 2;
                coefficient[2] -= 2;
                coefficient[4] += 1;
            }

            if (answer[1])
            {
                coefficient[0] += 1;
                coefficient[1] += 1;
                coefficient[2] += 1;
                coefficient[3] += 1;
            }

            if (answer[2])
            {
                coefficient[0] += 2;
                coefficient[1] += 2;
                coefficient[2] += 2;
            }

            return coefficient;
        }

        // Переподсчет коэффициентов при четвертом вопросе
        private int[] Get_Coefficient_Four_Question(bool[] answer, int[] coefficient)
        {
            if (answer[0])
            {
                coefficient[4] += 1;
            }

            if (answer[1])
            {
                coefficient[3] += 1;
            }

            if (answer[2])
            {
                coefficient[2] += 1;
            }

            if (answer[3])
            {
                coefficient[0] += 2;
                coefficient[1] += 1;
            }

            return coefficient;
        }

        // Переподсчет коэффициентов при пятом вопросе
        private int[] Get_Coefficient_Five_Question(bool[] answer, int[] coefficient)
        {
            if (answer[0])
            {
                coefficient[0] += 1;
                coefficient[1] += 2;
                coefficient[2] += 2;
                coefficient[4] -= 1;
            }

            if (answer[1])
            {
                coefficient[3] += 1;
                coefficient[4] += 1;
            }

            return coefficient;
        }

        // Переподсчет коэффициентов при шестом вопросе
        private int[] Get_Coefficient_Seven_Question(bool[] answer, int[] coefficient)
        {
            if (answer[0])
            {
                coefficient[3] += 2;
                coefficient[4] += 2;
            }

            if (answer[1])
            {
                coefficient[0] += 1;
                coefficient[1] += 1;
                coefficient[2] += 1;
            }

            if (answer[2])
            {
                coefficient[1] += 3;
            }

            return coefficient;
        }


    }
}
