﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Choice_of_programming_language
{
    public partial class Start_Form_View : Form
    {

        public Start_Form_View() => InitializeComponent();

        /* 
         * Нажатие
         * Кнопка "Ну че, стартуем!"
         */
        private void button1_Click_1(object sender, EventArgs e)
        {
            Hide();
            new Question_View().ShowDialog();
        }

        /* 
         * Нажатие 
         * Кнопка "Закрыть"
         */
        private void button2_Click_1(object sender, EventArgs e) => Application.Exit();

        private void Form1_Load(object sender, EventArgs e) { }

        private void button2_Click(object sender, EventArgs e) { }

        private void button1_Click(object sender, EventArgs e) { }

        private void label6_Click(object sender, EventArgs e) { }

        private void label3_Click(object sender, EventArgs e) { }

        private void label4_Click(object sender, EventArgs e) { }

        private void label5_Click(object sender, EventArgs e) { }

        private void panel1_Paint(object sender, PaintEventArgs e) { }

        private void label2_Click(object sender, EventArgs e) { }

        private void label1_Click(object sender, EventArgs e) { }

    }
}
