﻿using Choice_of_programming_language.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Choice_of_programming_language
{
    public class Questions_Controller
    {
        // Модель данных для тех кто не программировал ни разу
        Model_Qestion_One_Way model_Qestion_One_Way;

        // Модель данных для тех кто программировал пару раз
        Model_Qestion_Two_Way model_Qestion_Two_Way;

        // Модель данных для тех кто программист
        Model_Qestion_Three_Way model_Qestion_Three_Way;

        // Связь с формой
        private Question_View question_View;

        // Лист всех коэффициентов (на каждом этапе) - для кнопки Назад
        private List<int[]> all_Coefficient;

        // Флаг отвечающий за одновременный выбор
        private bool simultaneous_Selection;

        // Флаг отвечающий за последний вопрос
        private bool end;

        // Номер ответа по основному вопросу
        private int count_General_Question;

        // Номер вопроса
        private int count_Question;

        // Массив выбранных ответов
        private bool[] answer;

        // Массив коэффициентов
        private int[] coefficient;

        // Массив показа количества ответов
        private bool[] checks;

        // Массив заполнения вопроса и ответов
        private String[] names;

        // Конструктор класса
        public Questions_Controller(Question_View question_View)
        {
            this.question_View = question_View;                                                                                                         // Ссылка на форму

            Initial_Of_Element();                                                                                                                       // Инициализация переменных
        }

        //Инициализация переменных
        private void Initial_Of_Element()
        {
            this.count_General_Question = 0;                                                                                                            // Ответ на основной вопрос
            this.count_Question = 0;                                                                                                                    // Номер следующего вопроса

            this.end = false;                                                                                                                           // Флаг отвечающий за последний вопрос
            this.simultaneous_Selection = true;                                                                                                         // Флаг отвечающий за одновременный выбор ответов

            this.answer = new bool[6];
            for (int i = 0; i < this.answer.Length; i++) this.answer[i] = false;                                                                        // Инициализация массива коэффициентов

            this.coefficient = new int[5];
            for (int i = 0; i < coefficient.Length; i++) coefficient[i] = 0;                                                                            // Инициализация массива коэфициентов

            this.all_Coefficient = new List<int[]>();                                                                                                   // Инициализация листа ответов
            this.all_Coefficient.Add(this.coefficient);

            this.checks = new bool[6];
            for (int i = 0; i < this.checks.Length; i++) this.checks[i] = false;                                                                        // Инициализация массива показа количества ответов

            this.names = new String[7];
            for (int i = 0; i < this.names.Length; i++) names[i] = null;                                                                                // Инициализация массива заполнения вопроса и ответов

            // Инициализация моделей данных
            model_Qestion_One_Way = new Model_Qestion_One_Way(); 
            model_Qestion_Two_Way = new Model_Qestion_Two_Way();
            model_Qestion_Three_Way = new Model_Qestion_Three_Way();
        }

        // Основной метод класса
        public void Start(int count_General_Question, int count_Question, bool[] answer, bool end)
        {
            // Перезаписываем все приходящие переменный
            this.count_General_Question = count_General_Question;
            this.count_Question = count_Question;
            this.answer = answer;
            this.end = end;

            if (!end)
            {
                if (count_General_Question == 0)
                {
                    Start_Question();                                                                                                                   // Начальный вопрос
                }
                else
                {
                    Set_Next_Question();                                                                                                                // Следующий вопрос
                }
            }
            else
            {
                Update_Coefficient_For_End();                                                                                                           // Если это был последний вопрос
            }
        }

        // Функция показа последнего вопроса
        private void Update_Coefficient_For_End()
        {
            for (int i = 0; i < this.names.Length; i++) names[i] = "";                                                                                  // Обнуляем вопрос и ответы
            for (int i = 0; i < this.checks.Length; i++) this.checks[i] = false;                                                                        // Скрываем их

            Update_Coefficient();                                                                                                                       // Обновляем  коэффициенты (последний раз)
            this.all_Coefficient.Add(this.coefficient);                                                                                                 // Сохраняем ответы
            question_View.Update_Coefficient(this.coefficient);                                                                                         // Обновляем коэффициенты на форме
            question_View.Set_Text_For_Elements(this.names);                                                                                            // Обновляем элементы на форме
            question_View.Set_Visible_Check(this.checks);                                                                                               // Обновляем показ чекбоксов на форме
            question_View.Set_Visible_Result();                                                                                                         // Вывод ответа
        }

        // Функция подгружающая следующий вопрос
        private void Set_Next_Question()
        {
            if (count_General_Question == 1 && count_Question == 5) this.simultaneous_Selection = false;
            if (count_General_Question == 1 && count_Question == 5) this.end = true;
            if (count_General_Question == 2 && count_Question == 6) this.end = true;
            if (count_General_Question == 3 && count_Question == 4) this.end = true;

            // Обновляем коэффициенты и запоминаем результат
            if (count_Question != 1) Update_Coefficient();
            this.all_Coefficient.Add(this.coefficient);

            Update_Massive_Name();                                                                                                                      // Получаем данных со следующего вопроса
            Set_Checks();                                                                                                                               // Обновляем визуализацию чекбоксов
            question_View.Set_Update_Information(this.names, this.checks, this.coefficient, this.simultaneous_Selection, this.end);                     // Обновляем все данные на форме
        }

        // Фнукиця обновления коэфициентов
        private void Update_Coefficient()
        {
            if (count_General_Question == 1) this.coefficient = model_Qestion_One_Way.Get_Coefficient(count_Question, answer, coefficient);
            if (count_General_Question == 2) this.coefficient = model_Qestion_Two_Way.Get_Coefficient(count_Question, answer, coefficient);
            if (count_General_Question == 3) this.coefficient = model_Qestion_Three_Way.Get_Coefficient(count_Question, answer, coefficient);
        }

        // Функция получения данных со следующего вопроса
        private void Update_Massive_Name()
        {
            if (count_General_Question == 1) names = model_Qestion_One_Way.Set_Names_Massive(count_Question);
            if (count_General_Question == 2) names = model_Qestion_Two_Way.Set_Names_Massive(count_Question);
            if (count_General_Question == 3) names = model_Qestion_Three_Way.Set_Names_Massive(count_Question);
        }

        // Функция обновления визуализации чекбоксов
        private void Set_Checks()
        {
            for(int i = 1; i < this.names.Length; i++) {
                if (names[i] != null) this.checks[i-1] = true;
                    else this.checks[i-1] = false;
            }
        }

        // Функция запускающая основной вопрос
        private void Start_Question()
        {
            names[0] = "Вы когда нибудь программировали?";
            names[1] = "Нет";
            names[2] = "Пару раз";
            names[3] = "Да, я программист";
            names[4] = null;
            names[5] = null;
            names[6] = null;

            Set_Checks();                                                                                                                                   // Обновляем визуализацию чекбоксов

            question_View.Set_Update_Information(this.names, this.checks, this.coefficient, this.simultaneous_Selection, this.end);                         // Обновляем все данные на форме
        }
    }
}
